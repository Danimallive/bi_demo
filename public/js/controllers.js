'use strict';

/* Controllers */

angular.module('myApp.controllers', []).

  controller('MainCtrl',function ($scope,GetBalanceService,GetDeferredBalanceService){
    $scope.card = {
        retailer: "",
        pin: "",
        cardNumber: ""
    }

    $scope.requestId = ""

    $scope.results = []

    $scope.addToResults = function(data){
        $scope.results.push(data)
    }

    $scope.deferred = false

    $scope.toggleType = function(){
        $scope.deferred = !$scope.deferred;
    }



    $scope.response = ""

    $scope.findIndexOfResults = function(balanceId){
        for(var i = 0; i < $scope.results.length; i++)
        {
            if($scope.results[i].balanceId == balanceId)
            {
                return i
            }
        }
        return -1;
    }



    $scope.getDeferredBalance = function(index,balanceId){
        if (arguments.length == 0){
            balanceId = $scope.requestId;
            index = $scope.findIndexOfResults(balanceId);
            if (index == -1){
                $scope.addToResults({
                    balanceId:balanceId,
                    balance:"",
                    responseCode:"",
                    message:"",
                    retailer: "Deferred Balance"
                });
                index = $scope.results.length-1;
            }
        }
        $scope.response = GetDeferredBalanceService.getDeferredBalance(balanceId).then(function(result){
                if (result.data.responseCode != null){
                    $scope.results[index].balance = result.data.balance;
                    $scope.results[index].responseCode = result.data.responseCode;
                    $scope.results[index].message= result.data.responseMessage;

                }
                else{
                    result.data = "Try Again"
                }
                return result
            })
        }

    $scope.balance = function() {
        $scope.response = GetBalanceService.getBalance($scope.card).then(function(result){
            $scope.addToResults({
                retailer:$scope.card.retailer,
                pin:$scope.card.pin,
                cardNumber:$scope.card.cardNumber,
                balance:result.data.balance,
                deferred:result.data.responseCode == '010',
                balanceId:result.data.requestId,
                responseCode: result.data.responseCode,
                message: result.data.responseMessage
            })
            return result
        })
    }


    $scope.findRetailerName = function(retailerId){
        for(var i = 0; i < $scope.retailers.length; i++)
        {
            if($scope.retailers[i].id == retailerId)
            {
                return $scope.retailers[i].name;
            }

        }
        return retailerId;
    }

    $scope.retailers = [
        {"name":"A.C. Moore","id":90000000380},
        {"name":"A|X","id":90000000122},
        {"name":"Abercrombie & Fitch","id":90000000211},
        {"name":"Academy Sports","id":90000000437},
        {"name":"adidas","id":90000000123},
        {"name":"Aeropostale","id":90000000400},
        {"name":"Albertsons","id":90000000126},
        {"name":"ALDO","id":90000000314},
        {"name":"AMC Theatres","id":90000000208},
        {"name":"American Airlines","id":90000000002},
        {"name":"American Eagle Outfitters","id":90000000216},
        {"name":"Ann Taylor","id":90000000003},
        {"name":"Apple Store (not iTunes)","id":90000000004},
        {"name":"Applebee's","id":90000000209},
        {"name":"Arco","id":90000000235},
        {"name":"athleta","id":90000000027},
        {"name":"Babies R Us","id":90000000005},
        {"name":"Back Bay Restaurant Group","id":90000000375},
        {"name":"backcountry.com","id":90000000425},
        {"name":"Bahama Breeze","id":90000000100},
        {"name":"Baja Fresh","id":90000000135},
        {"name":"Banana Republic","id":90000000027},
        {"name":"Barnes & Noble","id":90000000007},
        {"name":"Barney's","id":90000000438},
        {"name":"Bass Pro Shops","id":90000000008},
        {"name":"Bath Body Works","id":90000000302},
        {"name":"bebe","id":90000000416},
        {"name":"Bed Bath & Beyond","id":90000000009},
        {"name":"Best Buy","id":90000000010},
        {"name":"Best Western","id":90000000101},
        {"name":"BJ's","id":90000000119},
        {"name":"Blockbuster","id":90000000102},
        {"name":"Bloomingdale's","id":90000000210},
        {"name":"Bob Evans","id":90000000103},
        {"name":"Bodybuilding.com","id":90000000429},
        {"name":"Bon-Ton","id":90000000315},
        {"name":"Bonefish Grill","id":90000000077},
        {"name":"Boston Market","id":90000000128},
        {"name":"BP","id":90000000235},
        {"name":"Brinker Restaurants","id":90000000012},
        {"name":"Brooks Brothers","id":90000000306},
        {"name":"Brookstone","id":90000000355},
        {"name":"Bubba Gump Shrimp Co.","id":90000000127},
        {"name":"Buca di Beppo","id":90000000138},
        {"name":"Buckle","id":90000000346},
        {"name":"Buffalo Wild Wings","id":90000000321},
        {"name":"Build-A-Bear Workshop","id":90000000104},
        {"name":"Burger King","id":90000000105},
        {"name":"Burlington","id":90000000440},
        {"name":"Burton","id":90000000316},
        {"name":"Cabela's","id":90000000013},
        {"name":"Cache","id":90000000317},
        {"name":"California Pizza Kitchen","id":90000000236},
        {"name":"Caribou Coffee","id":90000000407},
        {"name":"Carl's Jr.","id":90000000129},
        {"name":"carrabbas italian grill","id":90000000080},
        {"name":"CB2","id":90000000021},
        {"name":"champs sports","id":90000000025},
        {"name":"Chevron","id":90000000239},
        {"name":"Chico's","id":90000000347},
        {"name":"Chili's","id":90000000012},
        {"name":"Chipotle","id":90000000017},
        {"name":"Choice Hotels international","id":90000000421},
        {"name":"Claim Jumper","id":90000000018},
        {"name":"Coach","id":90000000019},
        {"name":"Cold Stone Creamery","id":90000000320},
        {"name":"Coldwater Creek","id":90000000403},
        {"name":"Coleman","id":90000000422},
        {"name":"Columbia","id":90000000324},
        {"name":"Competitive Cyclist","id":90000000424},
        {"name":"Converse","id":90000000130},
        {"name":"Cost Plus World Market","id":90000000121},
        {"name":"Costco","id":90000000345},
        {"name":"Cracker Barrel","id":90000000325},
        {"name":"Crate & Barrel","id":90000000021},
        {"name":"Crutchfield","id":90000000434},
        {"name":"Cub","id":90000000354},
        {"name":"CVS","id":90000000107},
        {"name":"Dairy Queen","id":90000000132},
        {"name":"Darden Restaurants","id":90000000022},
        {"name":"Dean & Deluca","id":90000000326},
        {"name":"Del Taco","id":90000000414},
        {"name":"Dell","id":90000000327},
        {"name":"Denny's","id":90000000244},
        {"name":"Dick's Sporting Goods","id":90000000381},
        {"name":"Disney Store","id":90000000221},
        {"name":"Domino's Pizza","id":90000000329},
        {"name":"Dooney Bourke","id":90000000131},
        {"name":"dressbarn","id":90000000364},
        {"name":"DSW","id":90000000023},
        {"name":"Dunkin' Donuts","id":90000000124},
        {"name":"EB Games","id":90000000328},
        {"name":"Einstein Bros. Bagels","id":90000000133},
        {"name":"Elephant Bar","id":90000000134},
        {"name":"Ethan Allen","id":90000000350},
        {"name":"Express","id":90000000311},
        {"name":"Exxon","id":90000000229},
        {"name":"f.y.e.","id":90000000026},
        {"name":"Finish Line","id":90000000313},
        {"name":"Fleming's","id":90000000080},
        {"name":"flemings outback bonefish carrabbas etc","id":90000000080},
        {"name":"Fogo De Chao","id":90000000371},
        {"name":"Foot Locker","id":90000000025},
        {"name":"Forever 21","id":90000000125},
        {"name":"Fossil","id":90000000367},
        {"name":"Four Seasons","id":90000000365},
        {"name":"Fred Meyer","id":90000000110},
        {"name":"FTD","id":90000000431},
        {"name":"GameStop","id":90000000220},
        {"name":"Gander Mountain","id":90000000357},
        {"name":"Gap","id":90000000027},
        {"name":"Gap Kids","id":90000000027},
        {"name":"Gap Options","id":90000000027},
        {"name":"GNC","id":90000000441},
        {"name":"Godiva Chocolatier","id":90000000030},
        {"name":"Groupon","id":90000000420},
        {"name":"GUESS","id":90000000031},
        {"name":"Guitar Center","id":90000000341},
        {"name":"H & M","id":90000000111},
        {"name":"Hat World","id":90000000363},
        {"name":"Hollister","id":90000000308},
        {"name":"Home Depot","id":90000000204},
        {"name":"HP","id":90000000412},
        {"name":"IHOP","id":90000000112},
        {"name":"In-N-Out Burger","id":90000000113},
        {"name":"J.Crew","id":90000000349},
        {"name":"Jamba Juice","id":90000000033},
        {"name":"Janie and Jack","id":90000000436},
        {"name":"JC Penney","id":90000000034},
        {"name":"JetBlue","id":90000000035},
        {"name":"Jiffy Lube","id":90000000114},
        {"name":"Jo-Ann Fabrics","id":90000000330},
        {"name":"Johnny Rockets","id":90000000376},
        {"name":"Jos. A. Bank","id":90000000409},
        {"name":"justice","id":90000000368},
        {"name":"kids foot locker","id":90000000025},
        {"name":"Kmart","id":90000000201},
        {"name":"Kohl's","id":90000000036},
        {"name":"Kroger","id":90000000037},
        {"name":"L.L.Bean","id":90000000038},
        {"name":"lady foot locker","id":90000000025},
        {"name":"Landry's Restaurants","id":90000000115},
        {"name":"Lands' End","id":90000000205},
        {"name":"Legal Sea Foods","id":90000000331},
        {"name":"LEGO","id":90000000318},
        {"name":"Lettuce Entertain You","id":90000000207},
        {"name":"Lids","id":90000000363},
        {"name":"Lone Star Steakhouse","id":90000000419},
        {"name":"LongHorn Steakhouse","id":90000000374},
        {"name":"Lord & Taylor","id":90000000309},
        {"name":"Lowe's","id":90000000203},
        {"name":"Lucky Brand Jeans","id":90000000342},
        {"name":"Macaroni Grill","id":90000000348},
        {"name":"Macy's","id":90000000202},
        {"name":"Marriott Travel Card","id":90000000039},
        {"name":"McCormick & Schmick's","id":90000000116},
        {"name":"McDonald's","id":90000000040},
        {"name":"Mens Wearhouse","id":90000000332},
        {"name":"Mobil","id":90000000229},
        {"name":"Morton's the Steakhouse","id":90000000117},
        {"name":"Neiman Marcus","id":90000000041},
        {"name":"New York & Company","id":90000000042},
        {"name":"Newegg","id":90000000415},
        {"name":"Nike","id":90000000322},
        {"name":"Nordstrom","id":90000000043},
        {"name":"Office Depot","id":90000000044},
        {"name":"OfficeMax","id":90000000045},
        {"name":"Old Navy","id":90000000027},
        {"name":"Olive Garden","id":90000000022},
        {"name":"Omaha Steaks","id":90000000048},
        {"name":"On the Border","id":90000000076},
        {"name":"Orvis","id":90000000351},
        {"name":"Outback Restaurant Brands","id":90000000080},
        {"name":"Outback Steakhouse","id":90000000080},
        {"name":"Overstock.com","id":90000000049},
        {"name":"P.F. Chang's","id":90000000054},
        {"name":"PacSun","id":90000000050},
        {"name":"Panda Express","id":90000000418},
        {"name":"Panera Bread","id":90000000372},
        {"name":"Payless","id":90000000333},
        {"name":"Peet's Coffee & Tea","id":90000000051},
        {"name":"PETCO","id":90000000052},
        {"name":"PetSmart","id":90000000053},
        {"name":"PGA Superstore","id":90000000334},
        {"name":"Pier 1 Imports","id":90000000214},
        {"name":"piperlime","id":90000000027},
        {"name":"Pottery Barn","id":90000000055},
        {"name":"Pottery Barn Kids","id":90000000055},
        {"name":"Pottery Barn Teen","id":90000000055},
        {"name":"Puma","id":90000000335},
        {"name":"Qdoba","id":90000000377},
        {"name":"Radio Shack","id":90000000402},
        {"name":"Ralph Lauren","id":90000000359},
        {"name":"RCWilley","id":90000000411},
        {"name":"Red Lobster","id":90000000022},
        {"name":"Red Robin","id":90000000240},
        {"name":"Regal Cinemas","id":90000000336},
        {"name":"Regal Entertainment","id":90000000336},
        {"name":"REI","id":90000000059},
        {"name":"Rite Aid","id":90000000060},
        {"name":"Rite Aid","id":90000000060},
        {"name":"Romanos Macaroni Grill","id":90000000348},
        {"name":"Roy's","id":90000000079},
        {"name":"rue21","id":90000000373},
        {"name":"Saks Fifth Avenue","id":90000000213},
        {"name":"Sam's Club","id":90000000071},
        {"name":"Sears","id":90000000201},
        {"name":"Sephora","id":90000000062},
        {"name":"Shell","id":90000000340},
        {"name":"Shoebuy.com","id":90000000433},
        {"name":"Sizzler","id":90000000378},
        {"name":"Sony","id":90000000338},
        {"name":"Southwest Airlines","id":90000000063},
        {"name":"Speedway","id":90000000423},
        {"name":"Sports Authority","id":90000000401},
        {"name":"Staples","id":90000000212},
        {"name":"Starbucks","id":90000000064},
        {"name":"Sunglass Hut","id":90000000065},
        {"name":"T.G.I. Fridays","id":90000000344},
        {"name":"Talbots","id":90000000430},
        {"name":"Target","id":90000000066},
        {"name":"Texaco Chevron","id":90000000239},
        {"name":"Texas Roadhouse","id":90000000360},
        {"name":"The Body Shop","id":90000000011},
        {"name":"The Capital Grille","id":90000000106},
        {"name":"The Children's Place","id":90000000015},
        {"name":"The Container Store","id":90000000020},
        {"name":"The Great Indoors","id":90000000370},
        {"name":"the land of nod","id":90000000021},
        {"name":"The Limited","id":90000000312},
        {"name":"The Melting Pot","id":90000000358},
        {"name":"The North Face","id":90000000307},
        {"name":"Ticketmaster","id":90000000305},
        {"name":"Tiffany & Co.","id":90000000067},
        {"name":"TigerDirect.com","id":90000000427},
        {"name":"Toys R Us","id":90000000068},
        {"name":"Tractor Supply Co.","id":90000000406},
        {"name":"United Artists","id":90000000336},
        {"name":"Urban Outfitters","id":90000000257},
        {"name":"US Airways","id":90000000069},
        {"name":"Victoria's Secret","id":90000000070},
        {"name":"Walgreens","id":90000000405},
        {"name":"Walmart","id":90000000071},
        {"name":"Walmart & Sam's Club","id":90000000071},
        {"name":"West Elm","id":90000000353},
        {"name":"White House Black Market","id":90000000319},
        {"name":"Whole Foods Market","id":90000000073},
        {"name":"Williams-Sonoma","id":90000000074},
        {"name":"Zales","id":90000000428},
        {"name":"Zappos.com","id":90000000404},
        {"name":"Zumiez","id":90000000361}
    ]
  });

