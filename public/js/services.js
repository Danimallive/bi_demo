'use strict';

/* Services */


// Demonstrate how to register services
// In this case it is a simple value service.
angular.module('myApp.services', []).
    factory("GetBalanceService",function($q, $http) {
        var getBalanceAPI = function(card) {
            console.log(card);
            return $http.post('/balance',{card:card});
        };

        return { getBalance: getBalanceAPI };
    })
    .factory("GetDeferredBalanceService",function($q, $http) {
        var getDeferredBalanceAPI = function(balanceId) {
            console.log("requestid = "+balanceId);
            return $http.get('/balance/'+balanceId);
        };

        return { getDeferredBalance: getDeferredBalanceAPI };
    });



