
/**
 * Module dependencies
 */

var express = require('express'),
  routes = require('./routes'),
  api = require('./routes/api'),
  http = require('http'),
  path = require('path'),
  crypto = require('crypto'),
  request = require('request'),
  querystring = require('querystring'),
  //passport = require('passport'),
  path = require('path');
;

var app = module.exports = express();

var API_KEY = "1YAW1vzJAf";
var API_USER = "35ylOpNUpS";
//var url = "https://api.cardflo.com";
var url = "https://bi-prod-aws.cardflo.com"
//var url = "https://54.208.195.198"
/**
 * Configuration
 */

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.engine('ejs', require('ejs-locals'));
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.static(path.join(__dirname, 'public')));
app.use(app.router);

// development only
if (app.get('env') === 'development') {
  app.use(express.errorHandler());
}

// production only
if (app.get('env') === 'production') {
  // TODO
};






function authenticate(method,contentType,canonicalized_resource, canonicalized_POST_Variables,callback){
    var string_to_sing = "";
    string_to_sing += method +"\n";
    string_to_sing += contentType +"\n";
    string_to_sing += new Date().toUTCString().replace("GMT", "+0000")+"\n";
    string_to_sing += canonicalized_POST_Variables+canonicalized_resource;
    //console.log(string_to_sing);
    var signature = crypto.createHmac('sha1', API_KEY).update(string_to_sing).digest('base64');
    return "CFA"+" "+API_USER+":"+signature;
}
//console.log(authenticate("GET","","/services/giftcardbalance/200000129",""));





function getDefferedBalance(balanceRequestId, callback){
    request(
        {
            method:'GET',
            uri: url+'/services/giftcardbalance/'+balanceRequestId,
            headers: {
                Authorization: authenticate("GET","","/services/giftcardbalance/"+balanceRequestId,""),
                Date: new Date().toUTCString().replace("GMT", "+0000")
            },
            strictSSL:false

        },
        function (err,response,body){
            callback(err,response,body)
        }
    )

}

function getBalance(retailer,card,pin,callback){

    var data_to_post ={
        retailerId:retailer,
        cardNumber:card,
        pin:pin
    }
    var resource = querystring.stringify(data_to_post)
    request(
        {
            method:'POST',
            uri: url+'/services/giftcardbalance',
            headers: {
                Authorization: authenticate("POST","application/x-www-form-urlencoded","/services/giftcardbalance",resource,""),
                Date: new Date().toUTCString().replace("GMT", "+0000"),
                'Content-Type': "application/x-www-form-urlencoded"
            },
            strictSSL:false,
            body:resource
        },
        function (err,response,body){
            callback(err,response,body);
        }
    )
}

/**
 * Routes
 */

// serve index and view partials
app.get('/', routes.index);
app.get('/partials/:name', routes.partials);

app.post('/balance', function(req,res){
    var card = req.body.card;
    //console.log(card);
    if (card.pan)
        card.cardNumber = card.pan;
    getBalance(card.retailer,card.cardNumber,card.pin,function(err,response,body){
        //console.log(body)
        res.send(body);
    });
});

app.get('/balance/:id', function(req,res){
    var balanceRequestId = req.params.id;
    //console.log(balanceRequestId);
    getDefferedBalance(balanceRequestId,function(err,response,body){
            //console.log(body)
            res.send(body);
    });
});


// JSON API
app.get('/api/name', api.name);

// redirect all others to the index (HTML5 history)
app.get('*', routes.index);


/**
 * Start Server
 */

http.createServer(app).listen(app.get('port'), function () {
  console.log('Express server listening on port ' + app.get('port'));
});
